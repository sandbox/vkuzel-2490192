<?php

/**
 * @file
 * Commerce GoPay payment processings.
 */

/**
 * Displays notification error message with a proper HTTP code.
 */
function _commerce_gopay_gw_display_error_and_exit($msg) {
  watchdog('commerce_gopay_gw', "Notification cannot be processed: @Message", array('@Message' => $msg), WATCHDOG_ERROR);
  header("HTTP/1.1 500 Internal Server Error");
  exit;
}

/**
 * Creates a refund transaction based on existing transaction.
 */
function _commerce_gopay_gw_create_refund_transaction($transaction, $gopay_payment) {
  $refund_transaction = clone $transaction;
  unset($refund_transaction->transaction_id, $refund_transaction->revision_id);
  $refund_transaction->amount = -$gopay_payment['amount'];
  $refund_transaction->currency = $gopay_payment['currency'];
  return $refund_transaction;
}

/**
 * Process notification sent by GoPay.
 */
function commerce_gopay_gw_notify() {
  if (empty($_GET['id'])) {
    _commerce_gopay_gw_display_error_and_exit(t('Query string parameter id has to be set!'));
  }
  $gopay_payment_id = $_GET['id'];

  watchdog('commerce_gopay_gw', 'GoPay notified us about payment @GoPayPaymentId.', array('@GoPayPaymentId' => $gopay_payment_id), WATCHDOG_INFO);

  $settings = commerce_gopay_gw_settings();
  $access_info = commerce_gopay_gw_get_access_info(
      $settings['environment'],
      $settings['client_id'],
      $settings['client_secret']
  );
  if (!$access_info) {
    _commerce_gopay_gw_display_error_and_exit(t('Cannot get an access information!'));
  }

  $payment_transaction_id = db_select('commerce_payment_transaction', 't')
    ->fields('t', array('transaction_id'))
    ->condition('t.remote_id', $gopay_payment_id)
    ->execute()
    ->fetchField();
  if (!$payment_transaction_id) {
    _commerce_gopay_gw_display_error_and_exit(t('A transaction for GoPay payment id @GoPayPaymentId not found!', array('@GoPayPaymentId' => $gopay_payment_id)));
  }

  $gopay_payment = commerce_gopay_gw_get_payment($settings['environment'], $access_info['access_token'], $gopay_payment_id);
  if (!$gopay_payment) {
    _commerce_gopay_gw_display_error_and_exit(t('GoPay payment @GoPayPaymentId cannot be retrieved!', array('@GoPayPaymentId' => $gopay_payment_id)));
  }

  $transaction = commerce_payment_transaction_load($payment_transaction_id);
  $transaction->remote_status = $gopay_payment['state'];
  switch ($gopay_payment['state']) {
    case 'CREATED':
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t("Payment created in GoPay it's going to be processed.");
      break;

    case 'PAYMENT_METHOD_CHOSEN':
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t('Payment method chosen.');
      break;

    case 'PAID':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t("Payment completed.");
      break;

    case 'AUTHORIZED':
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t('Payment authorized in GoPay.');
      break;

    case 'CANCELED':
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('Payment canceled.');
      break;

    case 'TIMEOUTED':
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('Payment timeouted.');
      break;

    case 'PARTIALLY_REFUNDED':
      watchdog('commerce_gopay_gw', 'Partial refundation of transaction @TransactionId (@Amount @Currency)', array(
          '@TransactionId' => $payment_transaction_id,
          '@Amount' => $gopay_payment['amount'],
          '@Currency' => $gopay_payment['currency'],
      ), WATCHDOG_INFO);
      $refund_transaction = _commerce_gopay_gw_create_refund_transaction($transaction, $gopay_payment);
      $refund_transaction->message = t('Partial refundation of transaction @TransactionId', array('@TransactionId' => $transaction->transaction_id));
      commerce_payment_transaction_save($refund_transaction);
      break;

    case 'REFUNDED':
      watchdog('commerce_gopay_gw', 'Refundation of transaction @TransactionId (@Amount @Currency)', array(
          '@TransactionId' => $payment_transaction_id,
          '@Amount' => $gopay_payment['amount'],
          '@Currency' => $gopay_payment['currency'],
      ), WATCHDOG_INFO);
      $refund_transaction = _commerce_gopay_gw_create_refund_transaction($transaction, $gopay_payment);
      $refund_transaction->message = t('Full refundation of transaction @TransactionId', array('@TransactionId' => $transaction->transaction_id));
      commerce_payment_transaction_save($refund_transaction);
      break;

    default:
      _commerce_gopay_gw_display_error_and_exit(t('Unsupported a GoPay payment status @Status!', array('@Status' => $gopay_payment['state'])));
  }
  commerce_payment_transaction_save($transaction);

  watchdog('commerce_gopay_gw', 'Transaction @TransactionId status has changed to @Status',
    array('@TransactionId' => $payment_transaction_id, '@Status' => $transaction->status), WATCHDOG_INFO);
  header("HTTP/1.1 200");
  if ($settings['environment'] == COMMERCE_GOPAY_GW_ENVIRONMENT_MOCKUP) {
    echo t('Your mockup payment has been processed. Please go back to <a href="@Url">e-shop</a> and check your order.', array('@Url' => url('', array('absolute' => TRUE))));
  }
  exit;
}
