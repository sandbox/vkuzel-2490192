<?php

/**
 * @file
 * Commerce GoPay settings form callback.
 */

/**
 * Returns form field definitions for one GoPay environment.
 */
function _commerce_gopay_gw_get_environment_dependent_fields($environment) {
  $settings = commerce_gopay_gw_settings($environment);
  $form = array();

  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('A vendor credentials settings for the GoPay payment gateway'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['credentials']['commerce_gopay_gw_' . $environment . '_goid'] = array(
    '#type' => 'textfield',
    '#title' => t('GoID'),
    '#description' => t('GoID identifies e-shop in GoPay.'),
    '#maxlength' => 20,
    '#size' => 20,
    '#default_value' => $settings['goid'],
  );
  $form['credentials']['commerce_gopay_gw_' . $environment . '_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#maxlength' => 20,
    '#size' => 20,
    '#default_value' => $settings['client_id'],
  );
  $form['credentials']['commerce_gopay_gw_' . $environment . '_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#maxlength' => 100,
    '#size' => 20,
    '#default_value' => $settings['client_secret'],
  );

  $form['gateway'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment gateway settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $payment_methods = commerce_gopay_gw_get_available_payment_methods();
  $form['gateway']['commerce_gopay_gw_' . $environment . '_selected_payment_methods'] = array(
    '#type' => 'select',
    '#title' => t('Available payment methods'),
    '#multiple' => TRUE,
    '#size' => min(10, count($payment_methods)),
    '#options' => $payment_methods,
    '#default_value' => $settings['selected_payment_methods'],
  );

  return $form;
}

/**
 * Returns settings form.
 */
function commerce_gopay_gw_settings_form($form, &$form_state) {
  $settings = commerce_gopay_gw_settings();
  $form['integration'] = array(
    '#type' => 'fieldset',
    '#title' => t('GoPay to e-shop integration setup'),
    '#collapsible' => FALSE,
  );
  $environment_options = array(
    COMMERCE_GOPAY_GW_ENVIRONMENT_MOCKUP => t("Mockup"),
    COMMERCE_GOPAY_GW_ENVIRONMENT_TEST => t("Test"),
    COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION => t("Production"),
  );
  $form['integration']['commerce_gopay_gw_environment'] = array(
    '#type' => 'select',
    '#title' => t('Environment (mockup, test, production)'),
    '#description' => t("Changing of this property won't affect existing orders or payments."),
    '#options' => $environment_options,
    '#default_value' => $settings['environment'],
  );

  $form[COMMERCE_GOPAY_GW_ENVIRONMENT_MOCKUP] = array(
    '#type' => 'fieldset',
    '#title' => t('Mockup environment settings (for "offline" testing)'),
    '#collapsible' => FALSE,
    '#states' => array(
      'visible' => array(
        'select[name="commerce_gopay_gw_environment"]' => array('value' => COMMERCE_GOPAY_GW_ENVIRONMENT_MOCKUP),
      ),
    ),
  );
  $form[COMMERCE_GOPAY_GW_ENVIRONMENT_MOCKUP] += _commerce_gopay_gw_get_environment_dependent_fields(COMMERCE_GOPAY_GW_ENVIRONMENT_MOCKUP);

  $form[COMMERCE_GOPAY_GW_ENVIRONMENT_TEST] = array(
    '#type' => 'fieldset',
    '#title' => t('Test environment settings'),
    '#collapsible' => FALSE,
    '#states' => array(
      'visible' => array(
        'select[name="commerce_gopay_gw_environment"]' => array('value' => COMMERCE_GOPAY_GW_ENVIRONMENT_TEST),
      ),
    ),
  );
  $form[COMMERCE_GOPAY_GW_ENVIRONMENT_TEST] += _commerce_gopay_gw_get_environment_dependent_fields(COMMERCE_GOPAY_GW_ENVIRONMENT_TEST);

  $form[COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION] = array(
    '#type' => 'fieldset',
    '#title' => t('Production settings'),
    '#collapsible' => FALSE,
    '#states' => array(
      'visible' => array(
        'select[name="commerce_gopay_gw_environment"]' => array('value' => COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION),
      ),
    ),
  );
  $form[COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION] += _commerce_gopay_gw_get_environment_dependent_fields(COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION);

  $form = system_settings_form($form);
  $form['#submit'][] = 'commerce_gopay_gw_settings_form_submit';

  return $form;
}

/**
 * Resets the list of payment methods on available payment methods change.
 */
function commerce_gopay_gw_settings_form_submit($form, &$form_state) {
  $original_environment = isset($form_state['complete form']['integration']['environment']['#default_value']) ?
      $form_state['complete form']['integration']['commerce_gopay_gw_environment']['#default_value'] : NULL;
  $original_payment_methods = isset($form_state['complete form'][$original_environment]['gateway']['commerce_gopay_gw_' . $original_environment . '_selected_payment_methods']['#default_value']) ?
      $form_state['complete form'][$original_environment]['gateway']['commerce_gopay_gw_' . $original_environment . '_selected_payment_methods']['#default_value'] : NULL;
  $environment = $form_state['values']['commerce_gopay_gw_environment'];
  $payment_methods = $form_state['values']['commerce_gopay_gw_' . $environment . '_selected_payment_methods'];

  if ($environment != $original_environment || $payment_methods != $original_payment_methods) {
    commerce_payment_modules_enabled(array('commerce_gopay_gw'));
  }
}
