<?php

/**
 * @file
 * GoPay API wrappers.
 */

define('COMMERCE_GOPAY_GW_TEST_API_URL', 'https://gw.sandbox.gopay.com/api');
define('COMMERCE_GOPAY_GW_PRODUCTION_API_URL', 'https://gate.gopay.cz/api');

/**
 * Performs a cURL request and parses response data.
 *
 * @param array $curl_options
 *   Associative array of CURLOPT_* options.
 *
 * @return bool|array
 *   Associative array of FALSE on failure.
 */
function _commerce_gopay_gw_curl_request(array $curl_options) {
  $ch = curl_init();
  curl_setopt_array($ch, $curl_options);
  $result = curl_exec($ch);

  if ($error_number = curl_errno($ch)) {
    watchdog('commerce_gopay_gw', 'cURL request to @Url failed due to error No: (@Error). Message: @Message',
      array(
        '@Url' => $curl_options[CURLOPT_URL],
        '@Error' => $error_number,
        '@Message' => curl_error($ch),
      ),
      WATCHDOG_ERROR
    );
    return FALSE;
  }
  return json_decode($result, TRUE);
}

/**
 * Calls GoPay OAUTH2 API and returns access information.
 *
 * @param string $environment
 *   Operating environment identified by on of the GOPAY_ENVIRONMENT_*
 *   constants.
 * @param string $client_id
 *   Access ID to REST API.
 * @param string $client_secret
 *   Access password to REST API.
 *
 * @return bool|array
 *   Associative array with access information or FALSE on failure.
 *
 * @see https://help.gopay.com/cs/tema/integrace-platebni-brany/integrace-nova-platebni-brany/postup-integrace-platebni-brany
 */
function commerce_gopay_gw_get_access_info($environment, $client_id, $client_secret) {
  switch ($environment) {
    case COMMERCE_GOPAY_GW_ENVIRONMENT_TEST:
      $api_url = COMMERCE_GOPAY_GW_TEST_API_URL . '/oauth2/token';
      break;

    case COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION:
      $api_url = COMMERCE_GOPAY_GW_PRODUCTION_API_URL . '/oauth2/token';
      break;

    default:
      return array(
        'token_type' => 'bearer',
        'access_token' => 'mockup_access_token',
        'expires_in' => 1800,
      );
  }

  $access_data = array(
    'grant_type' => 'client_credentials',
    'scope' => 'payment-create',
  );

  $curl_options = array(
    CURLOPT_URL => $api_url,
    CURLOPT_HTTPHEADER => array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
    ),
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_USERPWD => $client_id . ":" . $client_secret,
    CURLOPT_POSTFIELDS => http_build_query($access_data),
    CURLOPT_SSL_VERIFYPEER => FALSE,
  );

  $access_info = _commerce_gopay_gw_curl_request($curl_options);
  if (!empty($access_info['errors'])) {
    watchdog('commerce_gopay_gw', 'Access info cannot be acquired from @Url because @Message',
      array(
        '@Url' => $api_url,
        '@Message' => print_r($access_info, TRUE),
      ),
      WATCHDOG_ERROR
    );
    return FALSE;
  }
  return $access_info;
}

/**
 * Creates payment in GoPay and returns it's identification.
 *
 * @param string $environment
 *   Operating environment identified by on of the
 *   COMMERCE_GOPAY_GW_ENVIRONMENT_* constants.
 * @param string $access_token
 *   Access token created by the commerce_gopay_gw_get_access_info function.
 * @param array $payment_data
 *   Payment details.
 *
 * @return array|bool
 *   Payment identification or FALSE on error.
 */
function commerce_gopay_gw_create_payment($environment, $access_token, array $payment_data) {
  switch ($environment) {
    case COMMERCE_GOPAY_GW_ENVIRONMENT_TEST:
      $api_url = COMMERCE_GOPAY_GW_TEST_API_URL . '/payments/payment';
      break;

    case COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION:
      $api_url = COMMERCE_GOPAY_GW_PRODUCTION_API_URL . '/payments/payment';
      break;

    default:
      $id = mt_rand(100000, 999999);
      return array(
        'id' => $id,
        'order_number' => $payment_data['order_number'],
        'state' => 'CREATED',
        'amount' => $payment_data['amount'],
        'currency' => $payment_data['currency'],
        'payer' => array(
          'default_payment_instrument' => 'BANK_ACCOUNT',
          'allowed_payment_instruments' => ['BANK_ACCOUNT'],
          'default_swift' => 'FIOBCZPP',
          'contact' => array(
            'first_name' => 'Zbynek',
            'last_name' => 'Zak',
            'email' => ' zbynek.zak@gopay.cz ',
            'phone_number' => '+420777456123',
            'city' => 'C.Budejovice',
            'street' => 'Plana 67',
            'postal_code' => '37301',
            'country_code' => 'CZE',
          ),
        ),
        'target' => array(
          'type' => 'ACCOUNT',
          'goid' => 8123456789,
        ),
        'additional_params' => array(
          array(
            'name' => 'invoicenumber',
            'value' => '2015001003',
          ),
        ),
        'lang' => 'cs',
        'gw_url' => url('gopay/notify', array('query' => array('id' => $id), 'absolute' => TRUE)),
      );
  }

  $payment_data_json = json_encode($payment_data);

  $curl_options = array(
    CURLOPT_URL => $api_url,
    CURLOPT_HTTPHEADER => array(
      'Accept: application/json',
      'Content-Type: application/json',
      'Authorization: Bearer ' . $access_token,
    ),
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_POSTFIELDS => $payment_data_json,
    CURLOPT_SSL_VERIFYPEER => FALSE,
  );

  $payment = _commerce_gopay_gw_curl_request($curl_options);
  if (!empty($payment['errors'])) {
    watchdog('commerce_gopay_gw', 'Payment cannot be created on @Url because @Message',
      array(
        '@Url' => $api_url,
        '@Message' => print_r($payment, TRUE),
      ),
      WATCHDOG_ERROR
    );
    return FALSE;
  }
  return $payment;
}

/**
 * Returns payment information.
 *
 * @param string $environment
 *   Operating environment identified by on of the GOPAY_ENVIRONMENT_*
 *   constants.
 * @param string $access_token
 *   Access token created by the commerce_gopay_gw_get_access_info function.
 * @param string $gopay_payment_id
 *   Payment identification returned by the commerce_gopay_gw_create_payment
 *   function.
 *
 * @return array|bool
 *   Payment identification or FALSE on error.
 */
function commerce_gopay_gw_get_payment($environment, $access_token, $gopay_payment_id) {
  switch ($environment) {
    case COMMERCE_GOPAY_GW_ENVIRONMENT_TEST:
      $api_url = COMMERCE_GOPAY_GW_TEST_API_URL . '/payments/payment/' . $gopay_payment_id;
      break;

    case COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION:
      $api_url = COMMERCE_GOPAY_GW_PRODUCTION_API_URL . '/payments/payment/' . $gopay_payment_id;
      break;

    default:
      return array(
        'id' => 3000006529,
        'order_number' => '001',
        'state' => 'PAID',
        'amount' => 1000,
        'currency' => 'CZK',
        'payment_instrument' => 'BANK_ACCOUNT',
        'payer' => array(
          'contact' => array(
            'first_name' => 'Zbynek',
            'last_name' => 'Zak',
            'email' => ' zbynek.zak@gopay.cz ',
            'phone_number' => '+420777456123',
            'city' => 'C.Budejovice',
            'street' => 'Plana 67',
            'postal_code' => '37301',
            'country_code' => 'CZE',
          ),
        ),
        'target' => array(
          'type' => 'ACCOUNT',
          'goid' => 8123456789,
        ),
        'recurrence' => array(
          'recurrence_cycle' => 'DAY',
          'recurrence_period' => 7,
          'recurrence_date_to' => '2015-12-31',
          'recurrence_state' => 'REQUESTED',
        ),
        'additional_params' => array(
          array(
            'name' => 'invoicenumber',
            'value' => '2015001003',
          ),
        ),
        'lang' => 'cs',
        'gw_url' => url('gopay/notify', array('absolute' => TRUE)),
      );
  }

  $curl_options = array(
    CURLOPT_URL => $api_url,
    CURLOPT_HTTPHEADER => array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $access_token,
    ),
    CURLOPT_HTTPGET => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_SSL_VERIFYPEER => FALSE,
  );
  $payment = _commerce_gopay_gw_curl_request($curl_options);
  if (!empty($payment['errors'])) {
    watchdog('commerce_gopay_gw', 'Payment cannot be acquired from @Url because @Message',
      array(
        '@Url' => $api_url,
        '@Message' => print_r($payment, TRUE),
      ),
      WATCHDOG_ERROR
    );
    return FALSE;
  }
  return $payment;
}
