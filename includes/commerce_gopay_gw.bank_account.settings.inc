<?php

/**
 * @file
 * A GoPay bank account payment method settings form callback.
 */

/**
 * List of available swift codes.
 */
function _commerce_gopay_gw_bank_account_swifts() {
  return array(
    'GIBACZPX' => 'Česká spořitelna',
    'KOMBCZPP' => 'Komerční Banka',
    'RZBCCZPP' => 'Raiffeisenbank',
    'BREXCZPP' => 'mBank',
    'FIOBCZPP' => 'FIO Bank',
    'CEKOCZPP' => 'ČSOB',
    'CEKOCZPP-ERA' => 'ERA',
    'SUBASKBX' => 'Všeobecná úverová banka Banka',
    'TATRSKBX' => 'Tatra Banka',
    'UNCRSKBX' => 'Unicredit Bank SK',
    'GIBASKBX' => 'Slovenská spořitelna',
    'OTPVSKBX' => 'OTP Banka',
    'POBNSKBA' => 'Poštová Banka',
    'CEKOSKBX' => 'ČSOB SK',
    'LUBASKBX' => 'Sberbank Slovensko',
  );
}

/**
 * Fills settings form with default values.
 */
function _commerce_gopay_gw_bank_account_fill_default_settings(&$settings) {
  if (empty($settings['allowed_swifts'])) {
    $settings['allowed_swifts'] = array_keys(_commerce_gopay_gw_bank_account_swifts());
  }
  if (empty($settings['default_swift']) && !empty($settings['allowed_swifts'])) {
    $settings['default_swift'] = current($settings['allowed_swifts']);
  }
}

/**
 * Returns GoPay payment data specific for bank_account payment method.
 */
function commerce_gopay_gw_get_bank_account_payment_data($payment_method) {
  _commerce_gopay_gw_bank_account_fill_default_settings($payment_method['settings']);
  return array(
    'default_swift' => $payment_method['settings']['default_swift'],
    'allowed_swifts' => array_values($payment_method['settings']['allowed_swifts']),
  );
}

/**
 * Payment method callback: settings form.
 */
function commerce_gopay_gw_bank_account_settings_form($settings = NULL) {
  _commerce_gopay_gw_bank_account_fill_default_settings($settings);
  $swifts = _commerce_gopay_gw_bank_account_swifts();

  $form['default_swift'] = array(
    '#type' => 'select',
    '#title' => t('Default bank code'),
    '#options' => $swifts,
    '#default_value' => $settings['default_swift'],
  );
  $form['allowed_swifts'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Allowed bank codes'),
    '#options' => $swifts,
    '#default_value' => $settings['allowed_swifts'],
  );
  return $form;
}
