# commerce_gopay_gw
Description
-----------

This module integrates GoPay payment service to Drupal Commerce.

Requirements
------------

This module requires:

  - Drupal Commerce

Installation
------------

Download the module to your Drupal Commerce installation and enable it.

Configuration
-------------

Module's general configuration is available on address
admin/commerce/config/commerce_gopay_gw.

This module can operate in one of following three modes. Mockup is used for
a "offline" testing. Test mode communicates with GoPays testing environment.
And production used in real life.

Each GoPay enabled payment instrument is presented as a sole payment method on
a checkout page. So it's not necessary for customer to select payment method
twice.

Changes & bug fixes
-------------------

For changes/integration/fixes please contact author at email vkuzel@gmail.com

Module modifications won't be probably for free but I think we will figure out
some fair price.

Enjoy!
