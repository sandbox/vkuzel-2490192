<?php
/**
 * @file
 * GoPay payment gateway integration into the Drupal Commerce checkout process.
 */

define('COMMERCE_GOPAY_GW_ENVIRONMENT_MOCKUP', 'mockup');
define('COMMERCE_GOPAY_GW_ENVIRONMENT_TEST', 'test');
define('COMMERCE_GOPAY_GW_ENVIRONMENT_PRODUCTION', 'production');

/**
 * Returns all available payment methods.
 *
 * @return array
 *   Associative array where a key is payment method code and it's name is a
 *   value.
 */
function commerce_gopay_gw_get_available_payment_methods() {
  return array(
    'payment_card' => t('Payment Card (GoPay)'),
    'bank_account' => t('Bank Account Transfer (GoPay)'),
    'prsms' => t('Premium SMS (GoPay)'),
    'mpayment' => t('m-platba mobile phone payment (GoPay)'),
    'paysafecard' => t('Online payment by paysafecard (GoPay)'),
    'supercash' => t('SuperCASH payment (GoPay)'),
    'gopay' => t('GoPay account (GoPay)'),
    'paypal' => t('PayPal (GoPay)'),
  );
}

/**
 * Implements hook_menu().
 */
function commerce_gopay_gw_menu() {
  $items = array();

  $items['admin/commerce/config/commerce_gopay_gw'] = array(
    'title' => 'Commerce GoPay settings',
    'description' => 'Configure GoPay payment gateway integration.',
    'file' => 'includes/commerce_gopay_gw.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_gopay_gw_settings_form'),
    'access arguments' => array('administer commerce_gopay_gw'),
  );

  $items['gopay/notify'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'Commerce GoPay payment notification page',
    'file' => 'includes/commerce_gopay_gw.notify.inc',
    'page callback' => 'commerce_gopay_gw_notify',
    'access arguments' => array('access content'),
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function commerce_gopay_gw_init() {
  module_load_include('inc', 'commerce_gopay_gw', 'includes/commerce_gopay_gw.bank_account.settings');
  module_load_include('inc', 'commerce_gopay_gw', 'includes/commerce_gopay_gw.gopay_api');
}

/**
 * Returns module's settings.
 *
 * @param string $environment
 *   Operating environment identified by on of the
 *   COMMERCE_GOPAY_GW_ENVIRONMENT_* constants. If null then the current
 *   environment is used.
 *
 * @return array
 *   Settings associative array.
 */
function commerce_gopay_gw_settings($environment = NULL) {
  if ($environment == NULL) {
    $environment = variable_get('commerce_gopay_gw_environment', COMMERCE_GOPAY_GW_ENVIRONMENT_MOCKUP);
  }
  return array(
    'environment' => $environment,
    'goid' => variable_get('commerce_gopay_gw_' . $environment . '_goid'),
    'client_id' => variable_get('commerce_gopay_gw_' . $environment . '_client_id'),
    'client_secret' => variable_get('commerce_gopay_gw_' . $environment . '_client_secret'),
    'selected_payment_methods' => variable_get('commerce_gopay_gw_' . $environment . '_selected_payment_methods', array('payment_card')),
  );
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_gopay_gw_commerce_payment_method_info() {
  $settings = commerce_gopay_gw_settings();
  $available_payment_methods = commerce_gopay_gw_get_available_payment_methods();
  $selected_payment_methods = $settings['selected_payment_methods'];

  $payment_methods = array();
  foreach ($selected_payment_methods as $payment_method) {
    $payment_methods['commerce_gopay_gw_' . $payment_method] = array(
      'title' => $available_payment_methods[$payment_method],
      'short_title' => $available_payment_methods[$payment_method],
      'terminal' => FALSE,
      'offsite' => TRUE,
      'offsite_autoredirect' => FALSE,
      'active' => TRUE,
      'callbacks' => array(
        'redirect_form' => 'commerce_gopay_gw_redirect_form',
      ),
    );
  }

  return $payment_methods;
}

/**
 * Extracts payment method ID from payment method code.
 */
function _commerce_gopay_gw_get_payment_method_id($payment_method) {
  return strtoupper(substr($payment_method['method_id'], 18));
}

/**
 * Returns a payment data specific for a selected payment method.
 *
 * Function gathers data from a
 * commerce_gopay_gw_get_<payment_method_id>_payment_data method.
 */
function _commerce_gopay_gw_get_payment_method_specific_payment_data($payment_method) {
  $function = 'commerce_gopay_gw_get_' . _commerce_gopay_gw_get_payment_method_id($payment_method) . '_payment_data';
  if (function_exists($function)) {
    return $function($payment_method);
  }
  else {
    return array();
  }
}

/**
 * Returns reduced list of order items.
 */
function _commerce_gopay_gw_get_payment_items($order_wrapper) {
  $items = array();
  foreach ($order_wrapper->commerce_line_items as $line_item) {
    $properties = $line_item->getPropertyInfo();
    $amount = commerce_currency_amount_to_decimal($line_item->commerce_unit_price->amount->value(), $line_item->commerce_unit_price->currency_code->value());
    if ($amount == 0) {
      continue;
    }
    if (isset($properties['commerce_product']) && !empty($line_item->value()->commerce_product[LANGUAGE_NONE][0]['product_id'])) {
      $name = $line_item->commerce_product->title->value();
    }
    else {
      $name = $line_item->line_item_label->value();
    }
    $quantity = (int) $line_item->quantity->value();
    if ($quantity > 1) {
      $name .= " x " . $quantity;
    }
    $items[] = array(
      'amount' => $amount * $quantity,
      'name' => $name,
    );
  }
  return $items;
}

/**
 * Creates payment in GoPay.
 *
 * @param array $settings
 *   Module's settings returned by the commerce_gopay_gw_settings function.
 * @param array $access_info
 *   Access information returned by the commerce_gopay_gw_get_access_info
 *   function.
 * @param object $order
 *   Order object.
 * @param array $payment_method
 *   Payment method object.
 *
 * @return array
 *   Payment processed by GoPay.
 */
function commerce_gopay_gw_create_gopay_payment(array $settings, array $access_info, $order, array $payment_method) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_balance = commerce_payment_order_balance($order);
  global $language;
  $payment_data = array(
    'payer' => array(
      'default_payment_instrument' => _commerce_gopay_gw_get_payment_method_id($payment_method),
      'allowed_payment_instruments' => array(_commerce_gopay_gw_get_payment_method_id($payment_method)),
      'contact' => array(
        'email' => $order_wrapper->mail->value(),
      ),
    ),
    'target' => array(
      'type' => 'ACCOUNT',
      'goid' => $settings['goid'],
    ),
    'amount' => $order_balance['amount'],
    'currency' => $order_balance['currency_code'],
    'order_number' => $order->order_number,
    'order_description' => t('Order @order_number at @store', array(
      '@order_number' => $order->order_number,
      '@store' => variable_get('site_name', url('<front>', array('absolute' => TRUE))),
    )),
    'items' => _commerce_gopay_gw_get_payment_items($order_wrapper),
    'callback' => array(
      'notification_url' => url('gopay/notify', array('absolute' => TRUE)),
    ),
    'lang' => $language->language,
  );
  if (empty($order->data['payment_redirect_key'])) {
    $payment_data['callback']['return_url'] = url(NULL, array('absolute' => TRUE));
  }
  else {
    $payment_data['callback']['return_url'] = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
  }
  $payment_data['payer'] += _commerce_gopay_gw_get_payment_method_specific_payment_data($payment_method);

  return commerce_gopay_gw_create_payment($settings['environment'], $access_info['access_token'], $payment_data);
}

/**
 * Creates payment in GoPay and redirects a customer to gateway.
 *
 * Implements hook_redirect_form()
 */
function commerce_gopay_gw_redirect_form($form, &$form_state, $order, $payment_method) {
  $settings = commerce_gopay_gw_settings();
  $access_info = commerce_gopay_gw_get_access_info(
      $settings['environment'],
      $settings['client_id'],
      $settings['client_secret']
  );
  if ($access_info && $gopay_payment = commerce_gopay_gw_create_gopay_payment($settings, $access_info, $order, $payment_method)) {
    $transaction = commerce_payment_transaction_new('commerce_gopay_gw', $order->order_id);
    $transaction->uid = $order->uid;
    $transaction->instance_id = $payment_method['instance_id'];
    $transaction->payment_method = $payment_method['method_id'];
    $transaction->instance_id = $payment_method['instance_id'];
    $transaction->remote_id = $gopay_payment['id'];
    $order_balance = commerce_payment_order_balance($order);
    $transaction->amount = $order_balance['amount'];
    $transaction->currency_code = $order_balance['currency_code'];
    $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
    $transaction->message = t("Payment has been created in GoPay and will be processed.");
    $transaction->payload = $gopay_payment;
    commerce_payment_transaction_save($transaction);

    $form['#action'] = $gopay_payment['gw_url'];
    watchdog('commerce_gopay_gw', 'Redirect form to @Action', array('@Action' => $form['#action']), WATCHDOG_INFO);
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Proceed with payment'),
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'commerce_payment') . '/commerce_payment.js'),
      ),
    );
  }
  else {
    form_set_error('form', t('Payment cannot be created in GoPay!'));
    commerce_payment_redirect_pane_previous_page($order);
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Go back to payment selection'),
    );
  }

  return $form;
}
